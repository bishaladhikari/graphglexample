

package com.bishal.adhikari.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.bishal.adhikari.LoadAllPostsQuery
import java.util.*
class Converter {
    private val gson = Gson()
    @TypeConverter
    fun stringToList(data: String?): List<LoadAllPostsQuery.Data1> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<LoadAllPostsQuery.Data1>>() {

        }.type

        return gson.fromJson<List<LoadAllPostsQuery.Data1>>(data, listType)
    }

    @TypeConverter
    fun listToString(someObjects: List<LoadAllPostsQuery.Data1>): String {
        return gson.toJson(someObjects)
    }

}