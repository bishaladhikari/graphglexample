package com.rtchubs.filmadmin.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bishal.adhikari.R
import com.bishal.adhikari.databinding.ItemAlbumBinding
import com.bishal.adhikari.model.Photo
import com.bishal.etracker.mizanur.adapters.BaseAdapter


class PhotoListAdapter(
    var list: List<Photo>,
    val photoClickListener: (Photo) -> Unit = {}
) : BaseAdapter<Photo>(list) {

    override fun onCreateViewHolderBase(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PhotoViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_album, parent, false)
        )
    }

    override fun onBindViewHolderBase(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = (holder as PhotoListAdapter.PhotoViewHolder).binding
        list[position].let { data->
            binding?.tvAlbumtitle?.text = "Id: ${data.id}"
            binding?.tvAlbumDetails?.text = data.title ?: ""

            val url = GlideUrl(
                data.imageUrl, LazyHeaders.Builder()
                    .addHeader("User-Agent", WebSettings.getDefaultUserAgent(binding!!.ivAlbum.context))
                    .build()
            )

            binding?.ivAlbum?.let {
                Glide.with(it.context)
                    .load(url)
                    .centerCrop()
                    .placeholder(R.drawable.post_item_bg)
                    .into(binding.ivAlbum)
            }
        }
    }

    inner class PhotoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: ItemAlbumBinding? = DataBindingUtil.bind(view)

        init {
            itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    photoClickListener(list[adapterPosition])
                }
            })
        }
    }
}