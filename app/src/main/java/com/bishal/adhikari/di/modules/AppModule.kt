package com.bishal.adhikari.di.modules

import android.app.Application
import com.bishal.adhikari.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * AppModule will provide app-wide dependencies for a part of the application.
 * It should initialize objects used across our application, such as Room database, Retrofit, Shared Preference, etc.
 */

@Module
class AppModule {
    var mApplication =  App()

    fun AppModule(application: App){
        mApplication = application
    }

    @Provides
    @Singleton
    fun providesApplication(): Application {
        return mApplication
    }


}