package com.bishal.adhikari.di.modules

import android.content.Context
import com.bishal.adhikari.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ContextModule {

    @Provides
    @Singleton
    fun providesContext(): Context {
        return App()
    }

}