package com.bishal.adhikari.di.modules

import com.bishal.adhikari.ui.home.HomeActivity
import com.bishal.adhikari.ui.info.InfoActivity
import com.bishal.adhikari.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * All activities intended to use Dagger @Inject should be listed here.
 */
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [FragmentModule::class]) // Where to apply the injection.
    abstract fun contributeHomeActivity(): HomeActivity


    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun contributeInfoActivity(): InfoActivity
}