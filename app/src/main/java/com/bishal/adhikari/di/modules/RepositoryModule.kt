package com.bishal.adhikari.di.modules


import com.bishal.adhikari.data.data_source.DataSourceModule
import dagger.Module


@Module(includes = [DatabaseModule::class, DataSourceModule::class])
class RepositoryModule {
//    @Provides
//    @Singleton
//    internal fun provideRepository(postDao: PostDao, postRemoteDataSource: BaseRemoteDataSource): UserRepository{
//        return UserRepository(postDao, postRemoteDataSource)
//    }
}