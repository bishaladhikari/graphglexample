package com.bishal.adhikari.ui.fragments


import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.bishal.adhikari.R
import com.bishal.adhikari.data.data_source.Result
import com.bishal.adhikari.databinding.FragmentAlbumBinding
import com.bishal.adhikari.di.ViewModelFactory
import com.bishal.adhikari.ui.base.BaseFragment
import com.bishal.adhikari.ui.viewmodel.SharedViewModel
import com.bishal.adhikari.utils.GridItemDecoration
import com.rtchubs.filmadmin.adapters.PhotoListAdapter
import javax.inject.Inject


class AlbumFragment : BaseFragment<FragmentAlbumBinding>() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val viewModel: SharedViewModel by viewModels { viewModelFactory }


    override fun getLayoutRes(): Int = R.layout.fragment_album

    override fun setUpInitializers() {

    }

    override fun setUpListener() {
        initRecyclerConfig()
    }

    override fun setUpObservers() {
        viewModel.photos.observe(this, Observer { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    binding.progressBarAlbum.isVisible = false
                    result.data?.let {
                        val adapter = PhotoListAdapter(it) {}
                        binding.recyclerUser.adapter = adapter
                        adapter.notifyDataSetChanged()
                    }
                }
                Result.Status.LOADING -> binding.progressBarAlbum.isVisible = true
                Result.Status.ERROR -> {
                    binding.progressBarAlbum.isVisible = false
                    Snackbar.make(binding.root, result.message!!, Snackbar.LENGTH_LONG).show()
                }
            }
        })
    }

    override fun onCreate() {

    }

    private fun initRecyclerConfig(){
        with( binding.recyclerUser){
            setHasFixedSize(true)
//            layoutManager = GridLayoutManager(context, 2)
            addItemDecoration(GridItemDecoration(10, 2))
        }
    }
}