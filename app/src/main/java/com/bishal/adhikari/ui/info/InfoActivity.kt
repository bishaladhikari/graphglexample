package com.bishal.adhikari.ui.info


import com.bishal.adhikari.databinding.ActivityInfoBinding
import com.bishal.adhikari.ext.openWebPage
import com.bishal.adhikari.ui.base.BaseActivity

class InfoActivity : BaseActivity() {
    private lateinit var binding: ActivityInfoBinding

    override fun setUpLayoutBinding() {
        binding = ActivityInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }


    override fun setUpInitializers() {

        binding.button.setOnClickListener { openWebPage("https://www.linkedin.com/") }
        binding.button2.setOnClickListener { openWebPage("https://github.com/") }
        binding.button3.setOnClickListener { openWebPage("https://www.youtube.com/")
        }

    }

    override fun toolbarTitle(): CharSequence?  = "Bishal info"
}