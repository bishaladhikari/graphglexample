package com.bishal.adhikari.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bishal.adhikari.model.Photo

@Dao
interface PhotoDao {
    @Query("select * from photos ORDER BY id DESC")
    fun getAllPhotos() : LiveData<List<Photo>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(posts: List<Photo>)
}